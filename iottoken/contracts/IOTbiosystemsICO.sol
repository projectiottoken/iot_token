pragma solidity >=0.4.21 <0.6.0;

import "./IOTbiosystemsControlled.sol";
import "./Ownable.sol";
import "./TokenIOT.sol";

contract IOTbiosystemsICO is Ownable, Controller {
    uint256 constant public limit = 100 ether;
    uint256 constant public exchange = 1;
    uint256 public totalCollected;

    TokenIOT public tokenIOT;
    address public destEthers;

    
    constructor() public {
        totalCollected = 0;
    }


    modifier initialized() {
        require(address(tokenIOT) != address(0), "Not initialized");
        _;
    }


    function initialize(address _token, address _destEth) public {
        require(address(tokenIOT) == address(0), "Contract already initialized");
        tokenIOT = TokenIOT(_token);
        require(tokenIOT.totalSupply() == 0, "Contract already initialized");
        require(tokenIOT.controller() == address(this), "Contract already initialized");
        destEthers = _destEth;
    }

    
    function proxyPayment(address _th) public payable returns (bool success) {
        return doBuy(_th, msg.value);
    }


    function doBuy(address _sender, uint _amount) public returns (bool success) {
        uint256 tokensGenerated = _amount * exchange;

        require (totalCollected + _amount <= limit, "Limit reached");

        assert(tokenIOT.generateTokens(_sender, tokensGenerated));
        destEthers.transfer(_amount);

        totalCollected = totalCollected + _amount;

        return true;
    }
}