const IOTbiosystemsICO = artifacts.require("IOTbiosystemsICO");
const TokenIOT = artifacts.require("TokenIOT");

// Definimos que vamos a hacer los tests para el contrato
// MasterICO.

// Como podéis ver además se pasa por argumento una variable
// accounts, esta variable es un array de cuentas disponibles en 
// este entorno, en nuestro caso serán todas las que nos proporcione
// ganache.
contract("IOTbiosystemsICO", function(accounts) {
  // Cogemos del array de cuentas las cuentas que necesitemos
  // para el test
  const addressTokenHolder1 = accounts[0];
  const addressEther = accounts[3];

  let tokenIOT;
  let iOTbiosystemsICO;

  // Nuestro primer test consistirá en desplegar el contrato
  // de la ICO y el Token y configurar que la ICO será el controlador
  // del token.
  it("Deploys all contracts", async function() {
    tokenIOT = await TokenIOT.new();
    iOTbiosystemsICO = await IOTbiosystemsICO.new();

    await tokenIOT.changeController(iOTbiosystemsICO.address);

    await iOTbiosystemsICO.initialize(tokenIOT.address, addressEther);
  });

  // Hacemos la primera compra en la ICO y comprobamos si el 
  // balance de ETH lo hemos recibido donde esperamos
  // y además miramos si hemos recibido la cantidad de tokens
  // esperados
  it("Does the first buy", async function() {

    // Obtenemos los balances anteriores al realizar las operaciones,
    // hacemos esto para después poder computar cual es la diferencia
    // ya que ganache inicializa las cuentas con 100 ethers, por lo tanto
    // la cuenta donde van los tokens de la ICO tendrá 101 tokens después
    // de la primera ejecución, 102 en la segunda etc...
    const preBalance = await tokenIOT.balanceOf(addressTokenHolder1);
    const preTotalEth = await web3.eth.getBalance(addressEther);

    // Con `sendTransaction` mandamos un pago al contrato,
    // siempre que se manden ethers a un contrato tenemos que
    // hacerlo en unidades "wei", en este caso usamos el método
    // `toWei` de web3. `sendTransaction` además nos permite
    // especificar desde qué dirección queremos mandar los ethers
    await tokenIOT.sendTransaction({
      value: web3.toWei(1),
      gas: 300000,
      gasPrice: "20000000000",
      from: addressTokenHolder1
    });

    // Obtenemos los balances posteriores a las opeeraciones
    const postBalance = await tokenIOT.balanceOf(addressTokenHolder1);
    const postTotalEth = await web3.eth.getBalance(addressEther);
    
    // Calculamos la diferencia
    const currentBalance = postBalance - preBalance;
    const currentTotalEth = postTotalEth - preTotalEth;

    // Comprobamos que el valor obtenido es igual al esperado
    assert.equal(web3.fromWei(currentBalance), 1);
    assert.equal(web3.fromWei(currentTotalEth), 1);
  });
});
